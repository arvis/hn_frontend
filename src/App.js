import './App.css';
import React, { useMemo, useState, useEffect } from "react";
import Table from "./Table";
import axios from 'axios';

function App() {
  const [data, setData] = useState([]);

  // Using useEffect to call the API once mounted and set the data
  useEffect(() => {
    (async () => {
      const result = await axios("http://localhost:8000/json/");
      setData(result.data);
    })();
  }, []);

  const columns = useMemo(
    () => [
      {
        Header: 'Title',
        accessor: 'title',
      },
      {
        Header: 'Link',
        accessor: 'link',
      },
      {
        Header: 'Points',
        accessor: 'points',
      },
      {
        Header: 'Date created',
        accessor: 'date_created',
      },

    ],
    []
  );


  return (
    <div>
      <Table columns={columns} data={data} />
    </div>
  );
}

export default App;
