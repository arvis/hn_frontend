# About
This is the frontend for [HN scaper](https://gitlab.com/arvis/hn_scraper).

## Installation
Check out git project then run in project directory

### `npm install`

## Starting project
First of all, you need to be sure that [HN scaper](https://gitlab.com/arvis/hn_scraper) runs by checking [localhost](http://localhost:8000)
In the project directory, run:
### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

